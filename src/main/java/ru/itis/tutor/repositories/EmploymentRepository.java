package ru.itis.tutor.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.tutor.models.Employment;

@Repository
public interface EmploymentRepository extends JpaRepository<Employment, Long> {

}
