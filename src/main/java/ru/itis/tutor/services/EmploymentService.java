package ru.itis.tutor.services;


import ru.itis.tutor.dtos.EmploymentDto;

import java.util.List;

public interface EmploymentService {

    List<EmploymentDto> getAll();

}
