package ru.itis.tutor.services;

import ru.itis.tutor.dtos.TutorDto;

import java.util.List;

public interface TutorService {
    List<TutorDto> getAll();

    TutorDto getById(Long id);

    void delete(Long id);

}
