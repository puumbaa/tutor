package ru.itis.tutor.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.tutor.dtos.EmploymentDto;
import ru.itis.tutor.repositories.EmploymentRepository;
import ru.itis.tutor.services.EmploymentService;

import java.util.List;
import static ru.itis.tutor.dtos.EmploymentDto.from;

@Service
@RequiredArgsConstructor
public class EmploymentServiceImpl implements EmploymentService {

    private final EmploymentRepository employmentRepository;

    @Override
    public List<EmploymentDto> getAll() {
        return from(employmentRepository.findAll());
    }
}
