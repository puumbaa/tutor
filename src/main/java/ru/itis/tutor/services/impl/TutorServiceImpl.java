package ru.itis.tutor.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.tutor.dtos.TutorDto;
import ru.itis.tutor.exceptions.TutorNotFound;
import ru.itis.tutor.models.Tutor;
import ru.itis.tutor.repositories.TutorRepository;
import ru.itis.tutor.services.TutorService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TutorServiceImpl implements TutorService {

    private final TutorRepository tutorRepository;

    @Override
    public List<TutorDto> getAll() {
        return TutorDto.from(tutorRepository.findAll());
    }

    @Override
    public TutorDto getById(Long id) {
        return TutorDto.from(tutorRepository.findById(id).orElseThrow(() -> {
            throw new TutorNotFound("tutor: " + id + " not found");
        }));
    }

    @Override
    public void delete(Long id) {
        tutorRepository.deleteById(id);
    }

}



