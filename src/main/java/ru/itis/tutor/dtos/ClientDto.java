package ru.itis.tutor.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.tutor.models.Client;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto {
    private Long id;
    private String name;
    private String surname;

    public static ClientDto from(Client client){
        return ClientDto.builder()
                .id(client.getId())
                .name(client.getName())
                .surname(client.getSurname())
                .build();
    }

    public static List<ClientDto> from(List<Client> clients){
        return clients.stream()
                .map(ClientDto::from)
                .collect(Collectors.toList());
    }

}
