package ru.itis.tutor.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.tutor.models.Employment;
import ru.itis.tutor.models.EmploymentPhoto;
import ru.itis.tutor.models.Review;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmploymentDto {
    private Long id;
    private Long tutorId;
    private String description;
    private int price;
    private Employment.Category category;
    private Set<EmploymentPhoto> employmentPhotos;
    private Set<Review> reviews;

    public static EmploymentDto from(Employment employment) {
        return EmploymentDto.builder()
                .id(employment.getId())
                .tutorId(employment.getTutor().getId())
                .description(employment.getDescription())
                .price(employment.getPrice())
                .category(employment.getCategory())
                .employmentPhotos(employment.getEmploymentPhotos())
                .reviews(employment.getReviews())
                .build();
    }
    public static Set<EmploymentDto> from(Set<Employment> employments){
        return employments.stream()
                .map(EmploymentDto::from)
                .collect(Collectors.toSet());
    }

    public static List<EmploymentDto> from(List<Employment> employments){
        return employments.stream()
                .map(EmploymentDto::from)
                .collect(Collectors.toList());
    }
}
