package ru.itis.tutor.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.tutor.models.Tutor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TutorDto {
    private Long id;
    private String name;
    private String surname;
    private String experience;
    private String status;
    private String avatar;
    private Set<EmploymentDto> services;

    public static TutorDto from(Tutor tutor){
        return TutorDto.builder()
                .id(tutor.getId())
                .name(tutor.getName())
                .surname(tutor.getSurname())
                .experience(tutor.getExperience())
                .status(tutor.getStatus().name())
                .services(EmploymentDto.from(tutor.getEmployments()))
                .build();
    }
    public static List<TutorDto> from(List<Tutor> tutors){
        return tutors.stream()
                .map(TutorDto::from)
                .collect(Collectors.toList());
    }
}
