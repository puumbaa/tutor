package ru.itis.tutor.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"tutor","reviews"})
@ToString(exclude = {"tutor","reviews"})
public class Employment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "tutor_id")
    private Tutor tutor;
    private String description;
    private int price;
    @Enumerated(EnumType.STRING)
    private Category category;
    @OneToMany(mappedBy = "employment")
    private Set<Review> reviews;
    @OneToMany(mappedBy = "employment")
    private Set<EmploymentPhoto> employmentPhotos;

    public enum Category {
        //todo: define categories of service
    }

}
