package ru.itis.tutor.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"photos","reviews"},callSuper = true)
@ToString(exclude = {"photos","reviews"})
public class Client extends User{
    @OneToMany(mappedBy = "client")
    private Set<ClientPhoto> photos;
    @OneToMany(mappedBy = "client")
    private Set<Review> reviews;
}
