package ru.itis.tutor.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"employment", "client"})
@ToString(exclude = {"employment", "client"})
public class Review {
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "employment_id")
    private Employment employment;

    private int grade;
    private String comment;

    
}
