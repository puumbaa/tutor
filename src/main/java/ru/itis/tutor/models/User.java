package ru.itis.tutor.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@SuperBuilder
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Credentials credentials;
    private String name;
    private String surname;
    private LocalDate registered;
    @Enumerated(EnumType.STRING)
    private AccountState state;

    public enum AccountState{
        NOT_CONFIRMED, CONFIRMED, BANNED
    }

}
