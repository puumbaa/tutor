package ru.itis.tutor.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"employments","photos"}, callSuper = true)
@ToString(exclude = {"photos", "employments"})
public class Tutor extends User{



    private String experience;
    @Enumerated(EnumType.STRING)
    private TutorStatus status;
    @OneToMany(mappedBy = "tutor")
    private Set<Employment> employments;
    @OneToMany(mappedBy = "tutor")
    private Set<TutorPhoto> photos;

    public Tutor(Long id, Credentials credentials, String name, String surname, LocalDate registered, AccountState state) {
        super(id, credentials, name, surname, registered, state);
    }


    public enum TutorStatus {
        FREE, BUSY
    }

}
