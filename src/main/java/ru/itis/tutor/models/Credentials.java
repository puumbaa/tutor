package ru.itis.tutor.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;


@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Credentials {
    private String email;
    private String password;
}
