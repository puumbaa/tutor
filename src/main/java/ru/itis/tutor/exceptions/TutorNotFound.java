package ru.itis.tutor.exceptions;

public class TutorNotFound extends RuntimeException{
    public TutorNotFound(String message) {
        super(message);
    }
}
