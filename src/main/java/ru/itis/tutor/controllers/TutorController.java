package ru.itis.tutor.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.tutor.dtos.TutorDto;
import ru.itis.tutor.services.TutorService;

import java.util.List;


@RestController
@RequestMapping("/tutors")
@RequiredArgsConstructor
public class TutorController {

    private final TutorService tutorService;

    @GetMapping
    public ResponseEntity<List<TutorDto>> getAll(){
        return ResponseEntity.ok(tutorService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TutorDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(tutorService.getById(id));
    }

    @PostMapping
    public ResponseEntity<TutorDto> save(TutorDto newTutor){
       // todo: rewrite using AuthService  >>>  return tutorService.save(newTutor);
        return null;
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        tutorService.delete(id);
    }


}
