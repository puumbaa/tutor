package ru.itis.tutor.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.tutor.dtos.EmploymentDto;
import ru.itis.tutor.dtos.TutorDto;
import ru.itis.tutor.models.Employment;
import ru.itis.tutor.models.Tutor;
import ru.itis.tutor.services.EmploymentService;
import ru.itis.tutor.services.TutorService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/services")
public class EmploymentController {

    private final EmploymentService employmentService;
    private final TutorService tutorService;


    @GetMapping
    public String getServices(Model model){
        model.addAttribute("services",employmentService.getAll());
        return "services";
    }

    @PostMapping
    public String addEmployment(EmploymentDto employmentDto){

        TutorDto tutorDto = tutorService.getById(employmentDto.getTutorId());
        Tutor tutor = null;


        Employment employment = Employment.builder()
                .tutor(tutor)
                .build();

        return "redirect:/my-services";
    }
}
